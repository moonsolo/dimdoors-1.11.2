package StevenDimDoors.mod_pocketDimClient;

import StevenDimDoors.mod_pocketDim.config.DDProperties;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
//import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.event.world.WorldEvent;

public class ClientOnlyHooks {
    private DDProperties properties;

    private ISound limboMusic;
    private SoundCategory se;

    public ClientOnlyHooks(DDProperties properties) {
        this.properties = properties;
        this.se = SoundCategory.getByName("ambient");
        this.limboMusic = new PositionedSoundRecord(new ResourceLocation(mod_pocketDim.modid + ":creepy"), this.se, 1.0F, 1.0F, false, 0, ISound.AttenuationType.NONE, 0.0F, 0.0F, 0.0F);

    }


    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onSoundEffectResult(PlaySoundEvent event)
    {
        ResourceLocation playingSound = event.getSound().getSoundLocation();
        if (playingSound.getResourceDomain().equals("minecraft") && (playingSound.getResourcePath().equals("music.game") || playingSound.getResourcePath().equals("music.game.creative"))) {
            if (FMLClientHandler.instance().getClient().player.world.provider.getDimension() == mod_pocketDim.properties.LimboDimensionID) {
                ResourceLocation sound = new ResourceLocation(mod_pocketDim.modid + ":creepy");

                if (!Minecraft.getMinecraft().getSoundHandler().isSoundPlaying(limboMusic)) {
                    //event.result = limboMusic;
                    event.setResultSound(limboMusic);
                } else {
                    event.setResult(Event.Result.DENY);
                }
            }
        }
    }

    @SubscribeEvent
    public void onWorldLoad(WorldEvent.Load event) {
        if (event.getWorld().provider.getDimension() == mod_pocketDim.properties.LimboDimensionID &&
                event.getWorld().isRemote && !Minecraft.getMinecraft().getSoundHandler().isSoundPlaying(limboMusic)) {
            Minecraft.getMinecraft().getSoundHandler().playSound(limboMusic);
        }
    }
}
