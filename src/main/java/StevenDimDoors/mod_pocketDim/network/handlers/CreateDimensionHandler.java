package StevenDimDoors.mod_pocketDim.network.handlers;

import StevenDimDoors.mod_pocketDim.core.PocketManager;
import StevenDimDoors.mod_pocketDim.network.packets.CreateDimensionPacket;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CreateDimensionHandler implements IMessageHandler<CreateDimensionPacket, IMessage> {

    public CreateDimensionHandler() {}

    @Override
    public IMessage onMessage(CreateDimensionPacket message, MessageContext ctx) {
        PocketManager.getDimwatcher().onCreated(message.getDimensionData());

        return null;
    }
}
