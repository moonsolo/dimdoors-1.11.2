package StevenDimDoors.mod_pocketDim.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
//import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
//import net.minecraft.util.IIcon;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
//import StevenDimDoors.mod_pocketDimClient.PrivatePocketRender;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockDimWall extends Block
{
	private static final float SUPER_HIGH_HARDNESS = 10000000000000F;
	private static final float SUPER_EXPLOSION_RESISTANCE = 18000000F;
	//private IIcon[] blockIcon = new IIcon[3];
	
	public BlockDimWall(int j, Material par2Material)
	{
		super(par2Material);
		this.setCreativeTab(mod_pocketDim.dimDoorsCreativeTab);      
	}
	
	@Override
	public float getBlockHardness(IBlockState blockState, World worldIn, BlockPos pos)
	{
		if (this.getMetaFromState(worldIn.getBlockState(pos)) != 1)
		{
			return this.blockHardness;
		}
		else
		{
			return SUPER_HIGH_HARDNESS;
		}
	}
	
	@Override
    public float getExplosionResistance(Entity exploder)
    {
    	//tentative
		if (this.getMetaFromState(this.getDefaultState()) != 1)
		{
			return super.getExplosionResistance(exploder);
		}
		else
		{
			return SUPER_EXPLOSION_RESISTANCE;
		}
    }


    /*
	public int getRenderType()
    {
        return PrivatePocketRender.renderID;
    }
    */

    /*
	@Override
	public void registerBlockIcons(IIconRegister par1IconRegister)
    {
        this.blockIcon[0] = par1IconRegister.registerIcon(mod_pocketDim.modid + ":" + this.getUnlocalizedName());
        this.blockIcon[1] = par1IconRegister.registerIcon(mod_pocketDim.modid + ":" + this.getUnlocalizedName() + "Perm");
        this.blockIcon[2] = par1IconRegister.registerIcon(mod_pocketDim.modid + ":" + this.getUnlocalizedName() + "Personal");
    }
    */

    /*
	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int par1, int par2)
	{
		switch(par2)
		{
			case 0:
				return blockIcon[0];
			case 1:
				return blockIcon[1];
			case 2:
				return blockIcon[2];
			default:
			return blockIcon[0];
		}
	}
	*/

	@Override
	public int damageDropped(IBlockState state)
	{
		//Return 0 to avoid dropping Ancient Fabric even if the player somehow manages to break it
        int metadata = this.getMetaFromState(state);
		return metadata == 1 ? 0 : metadata;
	}
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item itemIn, CreativeTabs tab, NonNullList<ItemStack> list)
	{
		for (int ix = 0; ix < 3; ix++) 
		{
			list.add(new ItemStack(this, 1, ix));
		}
	}
	
    @Override
	public void onBlockDestroyedByPlayer(World worldIn, BlockPos pos, IBlockState state) {}
    
    @Override
	protected boolean canSilkHarvest()
    {
        return true;
    }
    
    @Override
	public int quantityDropped(Random par1Random)
    {
        return 0;
    }
   
    /**
     * replaces the block clicked with the held block, instead of placing the block on top of it. Shift click to disable. 
     */
    @Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
    	//Check if the metadata value is 0 -- we don't want the user to replace Ancient Fabric
        if (playerIn.getHeldItem(EnumHand.MAIN_HAND) != null && this.getMetaFromState(worldIn.getBlockState(pos)) != 1)
        {
        	Item playerEquip = playerIn.getHeldItem(EnumHand.MAIN_HAND).getItem();
        	
        	if (playerEquip instanceof ItemBlock)
        	{
        		// SenseiKiwi: Using getBlockID() rather than the raw itemID is critical.
        		// Some mods may override that function and use item IDs outside the range
        		// of the block list.

                ItemBlock playerEquipItemBlock = (ItemBlock)playerEquip;
        		Block block = playerEquipItemBlock.getBlock();
        		if (!block.isNormalCube(state, worldIn, pos) || block instanceof BlockContainer || block == this)
        		{
        			return false;
        		}
        		if (!worldIn.isRemote)
        		{
            		if (!playerIn.capabilities.isCreativeMode)
            		{
            			playerIn.getHeldItem(EnumHand.MAIN_HAND).setCount(playerIn.getHeldItem(EnumHand.MAIN_HAND).getCount() - 1);
            		}
            		worldIn.setBlockState(pos, block.getDefaultState(),0);
        		}
        		return true;
        	}
        }
        return false;
    }
}
