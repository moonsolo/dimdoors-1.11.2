package StevenDimDoors.mod_pocketDim.blocks;

import java.util.Random;

import StevenDimDoors.mod_pocketDim.core.LinkType;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
//import net.minecraft.client.renderer.IconFlipped;
//import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.audio.Sound;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDoor;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.IIcon;
//import net.minecraft.util.MathHelper;
//import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import StevenDimDoors.mod_pocketDim.config.DDProperties;
import StevenDimDoors.mod_pocketDim.core.DDTeleporter;
import StevenDimDoors.mod_pocketDim.core.DimLink;
import StevenDimDoors.mod_pocketDim.core.PocketManager;
import StevenDimDoors.mod_pocketDim.items.ItemDDKey;
import StevenDimDoors.mod_pocketDim.tileentities.TileEntityDimDoor;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class BaseDimDoor extends BlockDoor implements IDimDoor, ITileEntityProvider
{
    protected final DDProperties properties;
	
	//@SideOnly(Side.CLIENT)
    //protected IIcon[] upperTextures;
    //@SideOnly(Side.CLIENT)
    //protected IIcon[] lowerTextures;
	
	public BaseDimDoor(Material material, DDProperties properties)
	{
		super(material);
		
		this.properties = properties;
	}

	/*
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		upperTextures = new IIcon[2];
        lowerTextures = new IIcon[2];
        upperTextures[0] = iconRegister.registerIcon(mod_pocketDim.modid + ":" + this.getUnlocalizedName() + "_upper");
        lowerTextures[0] = iconRegister.registerIcon(mod_pocketDim.modid + ":" + this.getUnlocalizedName() + "_lower");
        upperTextures[1] = new IconFlipped(upperTextures[0], true, false);
        lowerTextures[1] = new IconFlipped(lowerTextures[0], true, false);
	}
	*/
	
    /**
     * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
     */
    /*
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata)
    {
        return upperTextures[0];
    }
    */

	@Override
	public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
	{
		this.enterDimDoor(worldIn, pos.getX(), pos.getY(), pos.getZ(), entityIn);
	}

	@Override
	//public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		
		ItemStack stack = playerIn.inventory.getCurrentItem();
		if (stack != null && stack.getItem() instanceof ItemDDKey)
		{
			return false;
		}

		if(!checkCanOpen(worldIn, pos.getX(), pos.getY(), pos.getZ(), playerIn))
		{
			return false;
		}

        int metadata = this.combineMetadata(worldIn, pos);
        int newMetadata = metadata & 7;
        newMetadata ^= 4;

        if ((metadata & 8) == 0)
        {
            worldIn.setBlockState(pos, this.getStateFromMeta(newMetadata), 2);
            worldIn.markBlockRangeForRenderUpdate(pos, pos);
        }
        else
        {
        	BlockPos p = new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ());
            worldIn.setBlockState(p, this.getStateFromMeta(newMetadata), 2);
            worldIn.markBlockRangeForRenderUpdate(p, pos);
        }

        //TODO: MOON
        //worldIn.playAuxSFXAtEntity(playerIn, 1003, x, y, z, 0);
        return true;
	}

	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
	{
		this.placeLink(worldIn, pos.getX(), pos.getY(), pos.getZ());
		worldIn.setTileEntity(pos, this.createNewTileEntity(worldIn, this.getMetaFromState(worldIn.getBlockState(pos))));
		this.updateAttachedTile(worldIn, pos.getX(), pos.getY(), pos.getZ());
	}

	/**
     * Retrieves the block texture to use based on the display side. Args: iBlockAccess, x, y, z, side
     */
	/*
	@Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side)
    {
        if (side != 1 && side != 0)
        {
            int fullMetadata = func_150012_g(blockAccess, x, y, z);
            int orientation = fullMetadata & 3;
            boolean reversed = false;

            if (isDoorOpen(fullMetadata))
            {
                if (orientation == 0 && side == 2)
                {
                    reversed = !reversed;
                }
                else if (orientation == 1 && side == 5)
                {
                    reversed = !reversed;
                }
                else if (orientation == 2 && side == 3)
                {
                    reversed = !reversed;
                }
                else if (orientation == 3 && side == 4)
                {
                    reversed = !reversed;
                }
            }
            else
            {
                if (orientation == 0 && side == 5)
                {
                    reversed = !reversed;
                }
                else if (orientation == 1 && side == 3)
                {
                    reversed = !reversed;
                }
                else if (orientation == 2 && side == 4)
                {
                    reversed = !reversed;
                }
                else if (orientation == 3 && side == 2)
                {
                    reversed = !reversed;
                }

                if ((fullMetadata & 16) != 0)
                {
                    reversed = !reversed;
                }
            }
            if (isUpperDoorBlock(fullMetadata))
            {
            	return this.upperTextures[reversed ? 1 : 0];
            }
            return this.lowerTextures[reversed ? 1 : 0];
        }
        return this.lowerTextures[0];
    }
    */

	//Called to update the render information on the tile entity. Could probably implement a data watcher,
	//but this works fine and is more versatile I think. 
	public BaseDimDoor updateAttachedTile(World world, int x, int y, int z)
	{
		BlockPos p = new BlockPos(x, y, z);
		mod_pocketDim.proxy.updateDoorTE(this, world, x, y, z);
		TileEntity tile = world.getTileEntity(p);
		if (tile instanceof TileEntityDimDoor)
		{
			int metadata = this.getMetaFromState(world.getBlockState(p));
			TileEntityDimDoor dimTile = (TileEntityDimDoor) tile;
			dimTile.openOrClosed = isDoorOnRift(world, x, y, z) && isUpperDoorBlock(metadata);
			dimTile.orientation = this.combineMetadata(world, p) & 7;
		}
		return this;
	}
	
	public boolean isDoorOnRift(World world, int x, int y, int z)
	{
		return this.getLink(world, x, y, z) != null;
	}
	
	public DimLink getLink(World world, int x, int y, int z)
	{
		DimLink link= PocketManager.getLink(x, y, z, world.provider.getDimension());
		if(link!=null)
		{
			return link;
		}

		BlockPos p = new BlockPos(x, y, z);
		if(isUpperDoorBlock( this.getMetaFromState(world.getBlockState(p))))
		{
			link = PocketManager.getLink(x, y-1, z, world.provider.getDimension());
			if(link!=null)
			{
				return link;
			}
		}
		else
		{
			link = PocketManager.getLink(x, y+1, z, world.provider.getDimension());
			if(link != null)
			{
				return link;
			}
		}
		return null;
	}

	/**
	 * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
	 * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		this.updateAttachedTile(worldIn, pos.getX(), pos.getY(), pos.getZ());
	}

	/*
	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess par1IBlockAccess, int par2, int par3, int par4)
	{
		this.setDoorRotation(func_150012_g(par1IBlockAccess, par2, par3, par4));
	}
	*/

	//TODO: MOON
	/*
	public void setDoorRotation(int par1)
	{
		float var2 = 0.1875F;
		//tentative
		//this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F);
		int var3 = par1 & 3;
		boolean var4 = (par1 & 4) != 0;
		boolean var5 = (par1 & 16) != 0;

		if (var3 == 0)
		{
			if (var4)
			{
				if (!var5)
				{
					this.setBlockBounds(0.001F, 0.0F, 0.0F, 1.0F, 1.0F, var2);
				}
				else
				{
					this.setBlockBounds(0.001F, 0.0F, 1.0F - var2, 1.0F, 1.0F, 1.0F);
				}
			}
			else
			{
				this.setBlockBounds(0.0F, 0.0F, 0.0F, var2, 1.0F, 1.0F);
			}
		}
		else if (var3 == 1)
		{
			if (var4)
			{
				if (!var5)
				{
					this.setBlockBounds(1.0F - var2, 0.0F, 0.001F, 1.0F, 1.0F, 1.0F);
				}
				else
				{
					this.setBlockBounds(0.0F, 0.0F, 0.001F, var2, 1.0F, 1.0F);
				}
			}
			else
			{
				this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, var2);
			}
		}
		else if (var3 == 2)
		{
			if (var4)
			{
				if (!var5)
				{
					this.setBlockBounds(0.0F, 0.0F, 1.0F - var2, .99F, 1.0F, 1.0F);
				}
				else
				{
					this.setBlockBounds(0.0F, 0.0F, 0.0F, .99F, 1.0F, var2);
				}
			}
			else
			{
				this.setBlockBounds(1.0F - var2, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
			}
		}
		else if (var3 == 3)
		{
			if (var4)
			{
				if (!var5)
				{
					this.setBlockBounds(0.0F, 0.0F, 0.0F, var2, 1.0F, 0.99F);
				}
				else
				{
					this.setBlockBounds(1.0F - var2, 0.0F, 0.0F, 1.0F, 1.0F, 0.99F);
				}
			}
			else
			{
				this.setBlockBounds(0.0F, 0.0F, 1.0F - var2, 1.0F, 1.0F, 1.0F);
			}
		}
	}
	*/

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which neighbor changed (coordinates passed are
	 * their own) Args: x, y, z, neighbor blockID
	 */
	//TODO: MOON: This method has been updated since creation and can be optimized. I'm just patching it to work for now
	@Override
	public void onNeighborChange(IBlockAccess world, BlockPos pos, BlockPos neighbor)
	{
        Block neighborBlock = (world.getBlockState(neighbor).getBlock());
		int metadata = this.getMetaFromState(world.getBlockState(pos));
        World w = world.getTileEntity(pos).getWorld();
		if (isUpperDoorBlock(metadata))
		{
			if (world.getBlockState(new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ())).getBlock() != this)
			{
				w.setBlockToAir(pos);
			}
			if (!neighborBlock.isAir(world.getBlockState(neighbor), world, pos) && neighborBlock != this)
			{
			    BlockPos p = new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ());
				this.onNeighborChange(world, p, neighbor);
			}
		}
		else
		{
		    Block lowerBlock = world.getBlockState(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ())).getBlock();
			if (lowerBlock != this)
			{
				w.setBlockToAir(pos);
				if (!w.isRemote)
				{
					this.dropBlockAsItem(w, pos, this.getStateFromMeta(metadata), 0);
				}
			}
			else if(this.getLockStatus(w, pos.getX(), pos.getY(), pos.getZ())<=1)
			{
				boolean powered = (w.isBlockIndirectlyGettingPowered(pos) > 0) || (w.isBlockIndirectlyGettingPowered(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ())) > 0);
				if ((powered || !neighborBlock.isAir(world.getBlockState(neighbor), world, pos) && neighborBlock.canProvidePower(neighborBlock.getDefaultState())) && neighborBlock != this)
				{
					this.toggleDoor(w, pos, powered);
				}
			}
		}
	}

	/**
	 * only called by clickMiddleMouseButton , and passed to inventory.setCurrentItem (along with isCreative)
	 */
	@Override
	@SideOnly(Side.CLIENT)
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
	{
		return new ItemStack(this.getDoorItem(), 1, 0);
	}

    /**
     * Returns the ID of the items to drop on destruction.
     */
    @Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return isUpperDoorBlock(this.getMetaFromState(state)) ? null : this.getDoorItem();
    }

    //TODO: MOON: ??? The hell is this overridden for
    /*
    @Override
    @SideOnly(Side.CLIENT)
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return this.getDoorItem();
    }
    */

	@Override
	public TileEntity createNewTileEntity(World world, int metadata)
	{
        return new TileEntityDimDoor();
	}

	@Override
	public void enterDimDoor(World world, int x, int y, int z, Entity entity) 
	{
		// FX entities dont exist on the server
		if (world.isRemote)
		{
			return;
		}
		
		// Check that this is the top block of the door
        BlockPos pos = new BlockPos(x, y - 1, z);
		if (world.getBlockState(pos) == this)
		{
			int metadata = this.getMetaFromState(world.getBlockState(pos));
			boolean canUse = isDoorOpen(metadata);
			if (canUse && entity instanceof EntityPlayer)
			{
				// Dont check for non-player entites
				canUse = isEntityFacingDoor(metadata, (EntityLivingBase) entity);
			}
			if (canUse)
			{
				// Teleport the entity through the link, if it exists
				DimLink link = PocketManager.getLink(x, y, z, world.provider.getDimension());
				if (link != null && (link.linkType() != LinkType.PERSONAL || entity instanceof EntityPlayer))
				{
					try
					{
						DDTeleporter.traverseDimDoor(world, link, entity, this);
					}
					catch (Exception e)
					{
						System.err.println("Something went wrong teleporting to a dimension:");
						e.printStackTrace();
					}
				}
				
				// Close the door only after the entity goes through
				// so players don't have it slam in their faces.
				this.toggleDoor(world, new BlockPos(x, y, z), false);
			}
		}
		else if (world.getBlockState(new BlockPos(x, y + 1, z)).getBlock() == this)
		{
			enterDimDoor(world, x, y + 1, z, entity);
		}
	}
	
	public boolean isUpperDoorBlock(int metadata)
	{
		return (metadata & 8) != 0;
	}
	
	public boolean isDoorOpen(int metadata)
	{
		return (metadata & 4) != 0;
	}
	
	/**
	 * 0 if link is no lock;
	 * 1 if there is a lock;
	 * 2 if the lock is locked.
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public byte getLockStatus(World world, int x, int y, int z)
	{
		byte status = 0;
		DimLink link = getLink(world, x, y, z);
		if(link!=null&&link.hasLock())
		{
			status++;
			if(link.getLockState())
			{
				status++;
			}
		}
		return status;
	}
	
	
	public boolean checkCanOpen(World world, int x, int y, int z)
	{
		return this.checkCanOpen(world, x, y, z, null);
	}
	
	public boolean checkCanOpen(World world, int x, int y, int z, EntityPlayer player)
	{
		DimLink link = getLink(world, x, y, z);
		if(link==null||player==null)
		{
			return link==null;
		}
		if(!link.getLockState())
		{
			return true;
		}
		
		for(ItemStack item : player.inventory.mainInventory)
		{
			if(item != null)
			{
				if(item.getItem() instanceof ItemDDKey)
				{
					if(link.tryToOpen(item))
					{
						return true;
					}
				}
			}
		}
		//tentative
		player.playSound(new SoundEvent(new ResourceLocation(mod_pocketDim.modid + ":doorLocked")),  1F, 1F);
		return false;
	}

	    
	protected static boolean isEntityFacingDoor(int metadata, EntityLivingBase entity)
	{
		// Although any entity has the proper fields for this check,
		// we should only apply it to living entities since things
		// like Minecarts might come in backwards.

        //tentative
		//int direction = MoonMathHelper.floor_double((entity.rotationYaw + 90) * 4.0F / 360.0F + 0.5D) & 3;
        double d = (entity.rotationYaw + 90) * 4.0F / 360.0F + 0.5D;
        int i = (int)d;
		int direction = d < (double)i ? i - 1 : i;
		return ((metadata & 3) == direction);
	}
	
	@Override
	public TileEntity initDoorTE(World world, int x, int y, int z)
	{
		TileEntity te = this.createNewTileEntity(world, this.getMetaFromState(world.getBlockState(new BlockPos(x, y, z))));
		world.setTileEntity(new BlockPos(x, y, z), te);
		return te;
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        //TODO: MOON: Does this work? I'm probably dumb for thinking this works
        //Store the original block
        Block oldBlock = worldIn.getBlockState(pos).getBlock();

		// This function runs on the server side after a block is replaced
		// We MUST call super.breakBlock() since it involves removing tile entities
        super.breakBlock(worldIn, pos, state);
        
        // Schedule rift regeneration for this block if it was replaced
        if (worldIn.getBlockState(pos).getBlock() != oldBlock)
        {
        	mod_pocketDim.riftRegenerator.scheduleFastRegeneration(pos.getX(), pos.getY(), pos.getZ(), worldIn);
        }
    }
}