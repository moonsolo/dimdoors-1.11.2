package StevenDimDoors.mod_pocketDim.blocks;

import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
//import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import StevenDimDoors.mod_pocketDim.config.DDProperties;
import StevenDimDoors.mod_pocketDim.core.DDTeleporter;
import StevenDimDoors.mod_pocketDim.core.DimLink;
import StevenDimDoors.mod_pocketDim.core.LinkType;
import StevenDimDoors.mod_pocketDim.core.NewDimData;
import StevenDimDoors.mod_pocketDim.core.PocketManager;

public class TransientDoor extends BaseDimDoor
{
	public TransientDoor(Material material, DDProperties properties)
	{
		super(material, properties);
	}

	@Override
	public void enterDimDoor(World world, int x, int y, int z, Entity entity) 
	{
		// We need to ignore particle entities
		if (world.isRemote)
		{
			return;
		}

		// Check that this is the top block of the door
		BlockPos p = new BlockPos(x, y - 1, z);
		BlockPos p2 = new BlockPos(x, y + 1, z);
		if (world.getBlockState(p).getBlock() == this)
		{
			boolean canUse = true;
			int metadata = this.getMetaFromState(world.getBlockState(p));
			if (canUse && entity instanceof EntityPlayer)
			{
				// Don't check for non-living entities since it might not work right
				canUse = BaseDimDoor.isEntityFacingDoor(metadata, (EntityLivingBase) entity);
			}
			if (canUse)
			{
				// Teleport the entity through the link, if it exists
				DimLink link = PocketManager.getLink(x, y, z, world.provider.getDimension());
				if (link != null)
				{
                    if (link.linkType() != LinkType.PERSONAL || entity instanceof EntityPlayer) {
                        DDTeleporter.traverseDimDoor(world, link, entity, this);
                        // Turn the door into a rift AFTER teleporting the player.
                        // The door's orientation may be necessary for the teleport.
                        world.setBlockState(new BlockPos(x, y, z), mod_pocketDim.blockRift.getDefaultState());
                        world.setBlockToAir(p);
                    }
				}
			}
		}
		else if (world.getBlockState(p2).getBlock() == this)
		{
			enterDimDoor(world, x, y + 1, z, entity);
		}
	}	

	@Override
	public void placeLink(World world, int x, int y, int z) 
	{
	    BlockPos p = new BlockPos(x, y - 1, z);
		if (!world.isRemote && world.getBlockState(p).getBlock() == this)
		{
			NewDimData dimension = PocketManager.createDimensionData(world);
			DimLink link = dimension.getLink(x, y, z);
			if (link == null && dimension.isPocketDimension())
			{
				dimension.createLink(x, y, z, LinkType.SAFE_EXIT, this.getMetaFromState(world.getBlockState(p)));
			}
		}
	}
	
	@Override
	public Item getDoorItem()
	{
		return null;
	}

	@Override
	public boolean isCollidable()
	{
		return false;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
	{
		return null;
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
}