package StevenDimDoors.mod_pocketDim.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import StevenDimDoors.mod_pocketDim.config.DDProperties;
import StevenDimDoors.mod_pocketDim.core.LinkType;
import StevenDimDoors.mod_pocketDim.core.NewDimData;
import StevenDimDoors.mod_pocketDim.core.PocketManager;

import java.util.ArrayList;
import java.util.Random;

public class UnstableDoor extends BaseDimDoor
{
	public UnstableDoor(Material material, DDProperties properties)
	{
		super(material, properties);
	}

	@Override
	public void placeLink(World world, int x, int y, int z) 
	{
		BlockPos p = new BlockPos(x, y - 1, z);
		if (!world.isRemote && world.getBlockState(p).getBlock() == this)
		{
			NewDimData dimension = PocketManager.getDimensionData(world);
			dimension.createLink(x, y, z, LinkType.RANDOM, this.getMetaFromState(world.getBlockState(p)));
		}
	}
	
	@Override
	public Item getDoorItem()
	{
		return mod_pocketDim.itemUnstableDoor;
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return Items.IRON_DOOR;
	}
}