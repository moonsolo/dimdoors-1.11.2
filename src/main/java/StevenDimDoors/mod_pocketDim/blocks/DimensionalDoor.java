package StevenDimDoors.mod_pocketDim.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import StevenDimDoors.mod_pocketDim.config.DDProperties;
import StevenDimDoors.mod_pocketDim.core.DimLink;
import StevenDimDoors.mod_pocketDim.core.LinkType;
import StevenDimDoors.mod_pocketDim.core.NewDimData;
import StevenDimDoors.mod_pocketDim.core.PocketManager;

public class DimensionalDoor extends BaseDimDoor
{
	public DimensionalDoor(Material material, DDProperties properties)
	{
		super(material, properties);
	}

	@Override
	public void placeLink(World world, int x, int y, int z) 
	{
		BlockPos p = new BlockPos(x, y - 1, z);
		if (!world.isRemote && world.getBlockState(p).getBlock() == this)
		{
			NewDimData dimension = PocketManager.createDimensionData(world);
			DimLink link = dimension.getLink(x, y, z);
			if (link == null)
			{
				dimension.createLink(x, y, z, LinkType.POCKET, this.getMetaFromState(world.getBlockState(p)));
			}
		}
	}
	
	@Override
	public Item getDoorItem()
	{
		return mod_pocketDim.itemDimensionalDoor;
	}
}