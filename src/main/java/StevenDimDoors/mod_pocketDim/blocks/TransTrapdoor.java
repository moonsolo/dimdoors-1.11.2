package StevenDimDoors.mod_pocketDim.blocks;

import java.util.Random;

import net.minecraft.block.state.BlockStateBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.BlockTrapDoor;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
//import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import StevenDimDoors.mod_pocketDim.core.DDTeleporter;
import StevenDimDoors.mod_pocketDim.core.DimLink;
import StevenDimDoors.mod_pocketDim.core.LinkType;
import StevenDimDoors.mod_pocketDim.core.NewDimData;
import StevenDimDoors.mod_pocketDim.core.PocketManager;
import StevenDimDoors.mod_pocketDim.items.ItemDDKey;
import StevenDimDoors.mod_pocketDim.tileentities.TileEntityTransTrapdoor;

public class TransTrapdoor extends BlockTrapDoor implements IDimDoor, ITileEntityProvider
{

	public TransTrapdoor(Material material)
	{
		super(material);
		this.setCreativeTab(mod_pocketDim.dimDoorsCreativeTab);
	}

	/*
	@Override
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		this.blockIcon = par1IconRegister.registerIcon(mod_pocketDim.modid + ":" + this.getUnlocalizedName());
	}
	*/

	//Teleports the player to the exit link of that dimension, assuming it is a pocket
	@Override
	public void onEntityCollidedWithBlock(World world, BlockPos pos, IBlockState state, Entity entity)
	{
		enterDimDoor(world, pos.getX(), pos.getY(), pos.getZ(), entity);
	}

	public boolean checkCanOpen(World world, int x, int y, int z)
	{
		return this.checkCanOpen(world, x, y, z, null);
	}
	
	public boolean checkCanOpen(World world, int x, int y, int z, EntityPlayer player)
	{
		DimLink link = PocketManager.getLink( x, y, z, world);
		if(link==null||player==null)
		{
			return link==null;
		}
		if(!link.getLockState())
		{
			return true;
		}
		
		for(ItemStack item : player.inventory.mainInventory)
		{
			if(item != null)
			{
				if(item.getItem() instanceof ItemDDKey)
				{
					if(link.tryToOpen(item))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
		if(this.checkCanOpen(worldIn, pos.getX(), pos.getY(), pos.getZ(), playerIn))
		{
			return super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ);
		}
		return false;
    }

    //TODO: MOON: This was never used?
    /*
    public void onPoweredBlockChange(World par1World, int par2, int par3, int par4, boolean par5)
    {
    	if(this.checkCanOpen(par1World, par2, par3, par4))
    	{
    		super.playSound(par1World, par2, par3, par4, par5);
    	}
    }
    */

	@Override
	public void enterDimDoor(World world, int x, int y, int z, Entity entity) 
	{
		if (!world.isRemote && ((this.getMetaFromState(world.getBlockState(new BlockPos(x, y, z)))) & 4) != 0)
		{
			DimLink link = PocketManager.getLink(x, y, z, world);
			if (link != null && (link.linkType() != LinkType.PERSONAL || entity instanceof EntityPlayer))
			{
				DDTeleporter.traverseDimDoor(world, link, entity,this);
			}
			if (entity instanceof EntityPlayer) {
                super.playSound((EntityPlayer)entity, world, new BlockPos(x, y, z), false);
            }
		}
	}	

	@Override
	public void onBlockAdded(World world, BlockPos pos, IBlockState state)
	{
		this.placeLink(world, pos.getX(), pos.getY(), pos.getZ());
		world.setTileEntity(pos, this.createNewTileEntity(world, this.getMetaFromState(world.getBlockState(pos))));
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int metadata)
	{
		return new TileEntityTransTrapdoor();
	}
	
	@Override
	public void placeLink(World world, int x, int y, int z) 
	{
		if (!world.isRemote)
		{
			NewDimData dimension = PocketManager.createDimensionData(world);
			DimLink link = dimension.getLink(x, y, z);
			if (link == null && dimension.isPocketDimension())
			{
				dimension.createLink(x, y, z, LinkType.UNSAFE_EXIT,0);
			}
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
	{
		return new ItemStack(this.getDoorItem(), 1, 0);
	}
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Item.getItemFromBlock(Blocks.TRAPDOOR);
    }
	
	@Override
	public Item getDoorItem()
	{
		return Item.getItemFromBlock(mod_pocketDim.transTrapdoor);
	}
	
	public static boolean isTrapdoorSetLow(int metadata)
	{
		return (metadata & 8) == 0;
	}
	
	@Override
	public TileEntity initDoorTE(World world, int x, int y, int z)
	{
		TileEntity te = this.createNewTileEntity(world, this.getMetaFromState(world.getBlockState(new BlockPos(x, y, z))));
		world.setTileEntity(new BlockPos(x, y, z), te);
		return te;
	}

	@Override
	public boolean isDoorOnRift(World world, int x, int y, int z)
	{
		return PocketManager.getLink(x, y, z, world)!=null;
	}
	
	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state)
    {
        //TODO: MOON: this thing again
        Block oldBlock = world.getBlockState(pos).getBlock();

		// This function runs on the server side after a block is replaced
		// We MUST call super.breakBlock() since it involves removing tile entities
        super.breakBlock(world, pos, state);
        
        // Schedule rift regeneration for this block if it was replaced
        if (world.getBlockState(pos) != oldBlock)
        {
        	mod_pocketDim.riftRegenerator.scheduleFastRegeneration(pos.getX(), pos.getY(), pos.getZ(), world);
        }
    }
}