package StevenDimDoors.mod_pocketDim.blocks;

import java.util.Random;
import StevenDimDoors.mod_pocketDim.mod_pocketDim;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class BlockDoorQuartz extends BlockDoor
{
	public BlockDoorQuartz(Material par2Material)
	{
		super(par2Material);
	}
	
	@SideOnly(Side.CLIENT)
    protected String getTextureName()
    {
        return mod_pocketDim.modid + ":" + this.getUnlocalizedName();
    }
	
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return (this.getMetaFromState(state) & 8) != 0 ? null : mod_pocketDim.itemQuartzDoor;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return new ItemStack(mod_pocketDim.itemQuartzDoor);
    }
}
